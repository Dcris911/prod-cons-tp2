package ca.qc.claurendeau.storage;


import java.util.ArrayList;
import java.util.List;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

import static ca.qc.claurendeau.utilitaire.Constante.MESSAGE_ERREUR_BUFFER_VIDE;
import static ca.qc.claurendeau.utilitaire.Constante.MESSAGE_ERREUR_BUFFER_REMPLI;

public class Buffer
{
	private int capacity;
	private List<Element> currentLoad;
	
    public Buffer(int capacity) {
        this.capacity = capacity;
        this.currentLoad = new ArrayList<Element>();
    }

    public int capacity() {
        return this.capacity;
    }

    public int getCurrentLoad() {
        return currentLoad.size();
    }

    public boolean isEmpty() {
    	return currentLoad.isEmpty();
    }

    public boolean isFull() {
    	return (getCurrentLoad() >= capacity());
    }

    public synchronized void addElement(Element element) throws BufferFullException {
    	if(isFull()) {
    		throw new BufferFullException(MESSAGE_ERREUR_BUFFER_REMPLI);
    	}
    	currentLoad.add(element);
    }
    
    public synchronized Element removeElement() throws BufferEmptyException {
    	if(isEmpty()) {
    		throw new BufferEmptyException(MESSAGE_ERREUR_BUFFER_VIDE);
    	}
    	return currentLoad.remove(positionDernierElement());
    }

    
    public String toString() {
        return "capacity:" + this.capacity + ", currentLoad:" + this.currentLoad;
    }

	private int positionDernierElement() {
		return getCurrentLoad()-1;
	}
}
