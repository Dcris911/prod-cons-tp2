package ca.qc.claurendeau.utilitaire;

public class Constante {
	
	public static final int TEST_VALEUR_DATA = 15;
	public static final int TEST_VALEUR_CAPACITE = 90;
	
	public static final int TEST_VALEUR_CURRENT_LOAD = 0;
	
	public static final String MESSAGE_ERREUR_BUFFER_VIDE = "Le buffer est vide";
	public static final String MESSAGE_ERREUR_BUFFER_REMPLI = "Le buffer est full";
}
