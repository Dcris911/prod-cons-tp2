package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {

	private static final long serialVersionUID = 5597037709742057295L;

	public BufferEmptyException() {}

	public BufferEmptyException(String message) {
		super(message);
	}
}
