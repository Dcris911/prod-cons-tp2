package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {

	private static final long serialVersionUID = -4560514347216852649L;

	public BufferFullException() {}

	public BufferFullException(String message) {
		super(message);
	}
    
}
