package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

import static ca.qc.claurendeau.utilitaire.Constante.TEST_VALEUR_CAPACITE;
import static ca.qc.claurendeau.utilitaire.Constante.TEST_VALEUR_DATA;
import static ca.qc.claurendeau.utilitaire.Constante.TEST_VALEUR_CURRENT_LOAD;

public class BufferTest {
	private Buffer buffer;
	
	@Before
	public void setup() {
		buffer = new Buffer(TEST_VALEUR_CAPACITE);
	}
	
    @Test
    public void testBuildSuccess() {
    	assertTrue(true);
    }
    
    @Test
    public void testToStringSansElementDansListe() {
        assertTrue(buffer.toString().equalsIgnoreCase("capacity:"+TEST_VALEUR_CAPACITE+", currentLoad:"+new ArrayList<>()));
    }
    
    @Test
    public void testCapacity() {
    	assertEquals(TEST_VALEUR_CAPACITE, buffer.capacity());
    }
    
    @Test
    public void testGetCurrentLoad() {
    	assertEquals(TEST_VALEUR_CURRENT_LOAD, buffer.getCurrentLoad());
    }
    
    @Test
    public void testIsEmptyListeVide() {
    	assertTrue(buffer.isEmpty());
    }
    
    @Test
    public void testIsFullListeVide() {
    	assertFalse(buffer.isFull());
    }
    
    @Test
    public void testAddElement() throws BufferFullException {
    	ajoutSimpleElementDansBuffer();
    	int nombreElementsDansBuffer = 1;
    	assertEquals(nombreElementsDansBuffer, buffer.getCurrentLoad());
    }
    
    @Test
    public void testIsEmptyListeAvecElements() throws BufferFullException {
    	ajoutSimpleElementDansBuffer();
    	assertFalse(buffer.isEmpty());
    }
    
     @Test
     public void testIsFullListeAvecElementsJusquaFull() throws BufferFullException {
    	 remplissageDuBufferJusqueMaxCapacite();
    	 assertTrue(buffer.isFull());
     }
     
     @Test(expected = BufferFullException.class)
     public void testAddElementQuandListeFull() throws BufferFullException {
    	 remplissageDuBufferJusqueMaxCapacite();
    	 ajoutSimpleElementDansBuffer();
     }
     
     @Test
     public void testRemoveElementRetireBienUnElement() 
    		 throws BufferFullException, BufferEmptyException {
    	 ajoutSimpleElementDansBuffer();
    	 retraitElement();
    	 int nombreElementsDansBuffer = 0;
    	 assertEquals(nombreElementsDansBuffer, buffer.getCurrentLoad());
     }
     
     @Test
     public void testRemoveElementRetourneBienLElementSupprimer() 
    		 throws BufferFullException, BufferEmptyException {
    	 ajoutSimpleElementDansBuffer();
    	 Element element = retraitElement();
    	 assertEquals(TEST_VALEUR_DATA, element.getData());
     }

     
     @Test(expected = BufferEmptyException.class)
     public void testRemoveElementQuandListeVide() 
    		 throws BufferFullException, BufferEmptyException {
    	 retraitElement();
     }
     
     @Ignore
     @Test
     public void testToStringAvecElementDansListe() {
         assertTrue(false);
     }
     
     private Element retraitElement() throws BufferEmptyException {
    	 return buffer.removeElement();
     }
     
     private void remplissageDuBufferJusqueMaxCapacite() throws BufferFullException {
    	 for(int i=0; i<TEST_VALEUR_CAPACITE; i++) {
       		ajoutSimpleElementDansBuffer();     		
       	}
     }

	private void ajoutSimpleElementDansBuffer() throws BufferFullException {
		Element element = new Element();
		element.setData(TEST_VALEUR_DATA);
		buffer.addElement(element);
	}
}


