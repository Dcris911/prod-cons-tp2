package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.Test;

import static ca.qc.claurendeau.utilitaire.Constante.TEST_VALEUR_DATA;

public class ElementTest {

	@Test
	public void testSetGetElement() {
		Element element = new Element();
		element.setData(TEST_VALEUR_DATA);

		assertEquals(TEST_VALEUR_DATA, element.getData());
	}

}
